package com.example.mywatchlist;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SearchResultsActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    String searchOption;
    String query;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_results);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Search Results");

        Intent resultIntent = getIntent();
        searchOption = resultIntent.getStringExtra("SEARCH_OPTION");
        query = resultIntent.getStringExtra("QUERY");
        Toast.makeText(getApplicationContext(),"Selected Search Option is " + searchOption + " and query is " + query
                ,Toast.LENGTH_LONG).show();

        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        // use a linear layout manager
        layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);

        if(searchOption.equals("Anime")) {
            new GetAnimeList().execute();
        }
    }

    private  class GetAnimeList extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Toast.makeText(getApplicationContext(), "Json Data is downloading", Toast.LENGTH_LONG).show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            final ArrayList<ArrayList<String>> animeList = new ArrayList<>();
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

            String URL_DATA = "https://api.jikan.moe/v3/search/anime?letter="+query.charAt(0)+"&q="+query+"&limit=50";
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                    URL_DATA,
                    null,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            try {

                                JSONArray jsonArray = response.getJSONArray("results");
                                if(jsonArray != null && jsonArray.length() <= 0) {
                                    Log.i("Empty", "Array is empty");
                                }

                                for (int j = 0; j < jsonArray.length(); j++) {
                                    ArrayList<String> jsonList = new ArrayList<>();
                                    JSONObject c = jsonArray.getJSONObject(j);
                                    String anime_title = c.getString("title").toLowerCase();
                                    String search_query = query.toLowerCase();

                                    if(subStringAtStart(search_query, anime_title)) {
                                        jsonList.add(c.getString("mal_id"));
                                        jsonList.add(c.getString("image_url"));
                                        jsonList.add(c.getString("title"));
                                        jsonList.add(c.getString("episodes"));
                                        jsonList.add(c.getString("synopsis"));
                                        jsonList.add(c.getString("airing"));
                                        jsonList.add(c.getString("score"));
                                        jsonList.add(c.getString("start_date"));
                                        jsonList.add(c.getString("end_date"));
                                        animeList.add(jsonList);
                                        Log.i("CONTENTS", jsonList.toString());
                                        mAdapter = new SearchResultsAdapter(animeList);
                                        recyclerView.setAdapter(mAdapter);
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("ERROR", error.getMessage(), error);

                        }
                    });

            requestQueue.add(jsonObjectRequest);


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
        }
    }

    public boolean subStringAtStart(String a, String b) {

        int count = 0;

        if(a == null || b == null) {
            return false;
        }
        if(a.length() > b.length()) {
            return false;
        }
        for(int i = 0; i < a.length(); i++) {
            if(a.charAt(i) == b.charAt(i)) {
                count++;
            }
        }
        if (count == a.length()) {
            return true;
        }

        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(Activity.RESULT_CANCELED);
                finish();
                break;
        }

        return true;
    }
}
