package com.example.mywatchlist;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mywatchlist.R;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

class SearchResultsAdapter extends RecyclerView.Adapter {

    private ArrayList<ArrayList<String>> dataset;
    Intent selectedIntent;

    public SearchResultsAdapter(ArrayList<ArrayList<String>>  list) {
        dataset = list;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageView;
        public TextView textView;
        public RelativeLayout relativeLayout;
        public MyViewHolder(View itemView) {
            super(itemView);
            this.imageView = (ImageView) itemView.findViewById(R.id.imageView);
            this.textView = (TextView) itemView.findViewById(R.id.textView);
            relativeLayout = (RelativeLayout)itemView.findViewById(R.id.relativeLayout);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = (View) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_row, parent, false);

        MyViewHolder vh = new MyViewHolder(v);
        selectedIntent = new Intent(v.getContext(), SelectedResultsActivity.class);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MyViewHolder newHolder = (MyViewHolder) holder;
        ArrayList<String> list = dataset.get(position);
        final ArrayList<String> sentList = list;
        final String name = list.get(2);

        newHolder.textView.setText(list.get(2));
        Picasso.with(newHolder.imageView.getContext())
                .load(list.get(1))
                .into(newHolder.imageView);

        newHolder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(),"Selected View is " + name,Toast.LENGTH_LONG).show();
                selectedIntent.putStringArrayListExtra("SELECTED_OPTION", sentList);
                v.getContext().startActivity(selectedIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }
}
