package com.example.mywatchlist;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.Toast;

public class SearchActivity extends AppCompatActivity {

    String search_selected;
    String search_query;
    Intent resultsIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        resultsIntent = new Intent(this, SearchResultsActivity.class);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        Intent searchIntent = getIntent();
        Toast.makeText(getApplicationContext(),"Selected Search Option is " + searchIntent.getStringExtra("SEARCH SELECTED"),Toast.LENGTH_LONG).show();
        actionBar.setTitle(searchIntent.getStringExtra("SEARCH SELECTED") + " Search");
        search_selected = searchIntent.getStringExtra("SEARCH SELECTED");

        SearchView searchView = (SearchView) findViewById(R.id.search_view);
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                search_query = removeSpaces(query);
                resultsIntent.putExtra("QUERY", search_query);
                resultsIntent.putExtra("SEARCH_OPTION", search_selected);
                startActivity(resultsIntent);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(Activity.RESULT_CANCELED);
                finish();
                break;
        }

        return true;
    }

    public String removeSpaces(String string) {

        if(string == null) {
            return " ";
        }

        String newString = string.replaceAll(" ", "");
        return newString;

    }
}
