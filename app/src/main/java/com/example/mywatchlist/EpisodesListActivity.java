package com.example.mywatchlist;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class EpisodesListActivity extends AppCompatActivity {

    private String ID;
    private String title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_episodes_list);

        Intent newIntent = getIntent();
        ID = newIntent.getStringExtra("ID");
        title = newIntent.getStringExtra("TITLE");

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(title);

        new getEpisodeList().execute();
    }

    private class getEpisodeList extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Toast.makeText(getApplicationContext(), "Json Data is downloading", Toast.LENGTH_LONG).show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            Log.i("MSGGGGGGG", "GOt here");
            final ArrayList<ArrayList<String>> episodeList = new ArrayList<>();
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

            String URL_DATA = "https://api.jikan.moe/v3/anime/" + ID + "/episodes";
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                    URL_DATA,
                    null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONArray jsonArray = response.getJSONArray("episodes");
                                if(jsonArray != null && jsonArray.length() <= 0) {
                                    Log.i("Empty", "Array is empty");
                                }

                                for(int i = 0; i < jsonArray.length(); i++) {
                                    ArrayList<String> jsonList = new ArrayList<>();
                                    JSONObject c = jsonArray.getJSONObject(i);

                                    jsonList.add(c.getString("episode_id"));
                                    jsonList.add(c.getString("title"));
                                    jsonList.add(c.getString("video_url"));
                                    Log.i("JSONLIST", jsonList.toString());
                                }

                            } catch (JSONException e) {

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("ERROR", error.getMessage(), error);
                        }
                    });

            requestQueue.add(jsonObjectRequest);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(Activity.RESULT_CANCELED);
                finish();
                break;
        }

        return true;
    }

}
