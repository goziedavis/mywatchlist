package com.example.mywatchlist;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;

public class SelectedResultsActivity extends AppCompatActivity {

    private ArrayList<String> info;
    Intent listIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_results);

        Intent selectedIntent = getIntent();
        info = selectedIntent.getStringArrayListExtra("SELECTED_OPTION");
        //Toast.makeText(getApplicationContext(),info.toString(),Toast.LENGTH_LONG).show();
        Log.i("SENT_ARRAY", info.toString());
        listIntent = new Intent(this, EpisodesListActivity.class);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(info.get(2));

        TextView textView = (TextView) findViewById(R.id.titleView);
        textView.setText(info.get(2));
        final ImageView imageView = (ImageView) findViewById(R.id.photoView);
        Picasso.with(imageView.getContext())
                .load(info.get(1))
                .into(imageView);
        TextView synopsisView = findViewById(R.id.synopsisView);
        synopsisView.setText(info.get(4));
        TextView episodeView = findViewById(R.id.episodeCount);
        episodeView.setText(info.get(3));
        TextView statusView = findViewById(R.id.statusView);
        if(info.get(5).equals("false")) {
            statusView.setText("Completed");
        }
        else{
            statusView.setText("Ongoing");
        }
        TextView scoreView = findViewById(R.id.scoreView);
        scoreView.setText(info.get(6));
        TextView dateView = findViewById(R.id.dateView);
        dateView.setText(getDate(info.get(7)) + " - " + getDate(info.get(8)));

        TextView listView = findViewById(R.id.viewEpisodesView);
        listView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listIntent.putExtra("ID", info.get(0));  //pass the mal id to the new intent (also pass the title)
                listIntent.putExtra("TITLE", info.get(2));
                v.getContext().startActivity(listIntent);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(Activity.RESULT_CANCELED);
                finish();
                break;
        }

        return true;
    }

    public String getDate(String date) {

        if(date == null) {
            return " ";
        }
        if(date == "null") {
            return " ";
        }

        if(date.length() < 10) {
            return " ";
        }
        return date.substring(0, 10);

    }
}
