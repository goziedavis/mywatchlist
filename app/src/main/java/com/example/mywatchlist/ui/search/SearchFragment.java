package com.example.mywatchlist.ui.search;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.mywatchlist.R;
import com.example.mywatchlist.SearchActivity;

import java.util.List;

public class SearchFragment extends Fragment {

    ListView listView;
    Intent searchIntent;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_search, container, false);

        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        searchIntent = new Intent(getContext(), SearchActivity.class);
        ArrayAdapter adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.Search_Options, android.R.layout.simple_list_item_1);

        listView = (ListView)  getView().findViewById(R.id.search_list); //findViewById(R.id.search_list);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(itemClick);

    }

    private AdapterView.OnItemClickListener itemClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            String itemValue = (String) listView.getItemAtPosition( position );
            searchIntent.putExtra("SEARCH SELECTED", itemValue);
            startActivity(searchIntent);

        }
    };

}
